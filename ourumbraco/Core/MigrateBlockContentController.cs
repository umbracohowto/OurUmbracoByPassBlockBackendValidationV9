﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Cache;
using Umbraco.Cms.Core.Logging;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.Blocks;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Infrastructure.Persistence;
using Umbraco.Cms.Web.Common;
using Umbraco.Cms.Web.Common.PublishedModels;
using Umbraco.Cms.Web.Website.Controllers;
using Umbraco.Extensions;

namespace OurUmbracoByPassBlockBackendValidationV9.Core
{
	public class MigrateBlockContentController : SurfaceController
	{
		private static readonly int numberOfRandomBlocksToInsert = 2;
		protected readonly UmbracoHelper _umbraco;
		
		public MigrateBlockContentController(
		   IUmbracoContextAccessor umbracoContextAccessor,
		   IUmbracoDatabaseFactory databaseFactory,
		   ServiceContext services,
		   AppCaches appCaches,
		   IProfilingLogger profilingLogger,
		   IPublishedUrlProvider publishedUrlProvider,
		   UmbracoHelper umbraco)
		   : base(umbracoContextAccessor, databaseFactory, services, appCaches, profilingLogger, publishedUrlProvider)
		{
			_umbraco = umbraco;
		}

		public IActionResult Index()
		{
			var frontPageId = 1061;
			var frontPage = _umbraco.Content(frontPageId);
			var existingBlocks = frontPage.Value<BlockListModel>("frontPageBlocks");
			var contentService = Services.ContentService; //if the class containing our code inherits SurfaceController, UmbracoApiController, or UmbracoAuthorizedApiController, we can get ContentService from Services namespace
			IContentTypeService contentTypeService = Services.ContentTypeService;  //not to be confused with ContentService, this service will be useful for getting some Document Type IDs
			Blocklist blocklistNew = new Blocklist();
			IContent request = contentService.GetById(frontPageId);
			var newBlockList = new List<Dictionary<string, string>>();
			var contentTypes = contentTypeService.GetAll();  //we get the Content Types to later get the Person Document Type key from
			var personType = contentTypes.Where(n => n.Alias == "myBlock").FirstOrDefault();  //using the above types, we locate the one that corresponds to the Person Document Type
			var dictionaryUdi = new List<Dictionary<string, string>>();  //the dictionaryUdi list here will be passed in the first section of our final JSON object
			foreach (var block in existingBlocks)
			{
				if (block?.ContentUdi == null) { continue; }
				var blockContent = (MyBlock)block.Content;
				GuidUdi contentUdi = new GuidUdi("element", System.Guid.NewGuid());
				newBlockList.Add(new Dictionary<string, string> //add person1
				{
					{"contentTypeKey", personType.Key.ToString()},  //we need to pass the key of the Block List item type, we used ContentTypeService to obtain it
					{"udi", contentUdi.ToString()},  //each item should also have a unique udi. We are passing the one we generated before
					{"mandatoryName", blockContent.MandatoryName},  //Document Type custom property
					{"title", blockContent.Title}  //Document Type custom property
				});
				dictionaryUdi.Add(new Dictionary<string, string> { { "contentUdi", contentUdi.ToString() } });
				
			}
			


			for (int i = 0; i < numberOfRandomBlocksToInsert; i++)
			{
				var block = new MyBlockElement() { MandatoryName = "", Title = $"{i}) {Guid.NewGuid()}" };
				GuidUdi contentUdi = new GuidUdi("element", System.Guid.NewGuid());
				newBlockList.Add(new Dictionary<string, string> //add person1
				{
					{"contentTypeKey", personType.Key.ToString()},  //we need to pass the key of the Block List item type, we used ContentTypeService to obtain it
					{"udi", contentUdi.ToString()},  //each item should also have a unique udi. We are passing the one we generated before
					{"mandatoryName", block.MandatoryName},  //Document Type custom property
					{"title", block.Title}  //Document Type custom property
				});
				dictionaryUdi.Add(new Dictionary<string, string> { { "contentUdi", contentUdi.ToString() } });
				//Write($"Attempting to insert {block.Title}");
				Thread.Sleep(500);
			}
			blocklistNew.layout = new BlockListUdi(dictionaryUdi);  //first section of JSON must contain udi references to whatever is in contentData
			blocklistNew.contentData = newBlockList;  //contentData is a list of our Person objects
			blocklistNew.settingsData = new List<Dictionary<string, string>>();  //If your Block List Item has settings, you would use the same technique as for contentData to set the setting values here, based on the underlying settings Document Type. If there are no settings for the item, we still need to set an empty list here to avoid a null error.
			request.SetValue("frontPageBlocks", JsonConvert.SerializeObject(blocklistNew));  //bind the serialized JSON data to our property alias, "people"
			var publishResult = contentService.SaveAndPublish(request);  //save and publish the node

			return Content($"{publishResult.Result}");
		}
	}

	public class Blocklist //this class is to mock the correct JSON structure when the object is serialized https://github.com/umbraco/UmbracoDocs/blob/e64ec0e5b28b4e5a37b7865691621e45dd82701f/Getting-Started/Backoffice/Property-Editors/Built-in-Property-Editors/Block-List-Editor/index.md
	{
		public BlockListUdi layout { get; set; }
		public List<Dictionary<string, string>> contentData { get; set; }
		public List<Dictionary<string, string>> settingsData { get; set; }
	}
	public class BlockListUdi //this is a subclass which corresponds to the "Umbraco.BlockList" section in JSON
	{
		[JsonProperty("Umbraco.BlockList")]  //we mock the Umbraco.BlockList name with JsonPropertyAttribute to match the requested JSON structure
		public List<Dictionary<string, string>> contentUdi { get; set; }

		public BlockListUdi(List<Dictionary<string, string>> items)
		{
			this.contentUdi = items;
		}
	}
	public class MyBlockElement
	{
		public string MandatoryName { get; set; }
		public string Title { get; set; }
	}

}