﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Umbraco.Cms.Core.Composing;
using Umbraco.Cms.Core.DependencyInjection;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Persistence.Repositories;
using Umbraco.Cms.Core.PropertyEditors;
using Umbraco.Cms.Core.Scoping;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Services.Implement;
using Umbraco.Cms.Core.Strings;
using Umbraco.Cms.Web.BackOffice.Controllers;
namespace OurUmbracoByPassBlockBackendValidationV9.Core.Composers
{
	public class UnvalidatedContentComposer : IComposer
	{
		public void Compose(IUmbracoBuilder builder) => builder.Services.AddSingleton<UnvalidatedContentService>();
	}

	public class UnvalidatedContentController : UmbracoAuthorizedApiController
	{
		private readonly IContentService _contentService;

		public UnvalidatedContentController(UnvalidatedContentService contentService) => _contentService = contentService;

		public PublishResult SaveAndPublish(int id) => _contentService.SaveAndPublish(_contentService.GetById(id));
	}

	public class UnvalidatedContentService : ContentService
	{
		public UnvalidatedContentService(IScopeProvider provider, ILoggerFactory loggerFactory, IEventMessagesFactory eventMessagesFactory, IDocumentRepository documentRepository, IEntityRepository entityRepository, IAuditRepository auditRepository, IContentTypeRepository contentTypeRepository, IDocumentBlueprintRepository documentBlueprintRepository, ILanguageRepository languageRepository, IShortStringHelper shortStringHelper)
			: base(provider, loggerFactory, eventMessagesFactory, documentRepository, entityRepository, auditRepository, contentTypeRepository, documentBlueprintRepository, languageRepository, new Lazy<IPropertyValidationService>(() => new NoopPropertyValidationService()), shortStringHelper)
		{ }

		private class NoopPropertyValidationService : IPropertyValidationService
		{
			public bool IsPropertyDataValid(IContent content, out IProperty[] invalidProperties, CultureImpact impact)
			{
				invalidProperties = null;
				return true;
			}

			public bool IsPropertyValid(IProperty property, string culture = "*", string segment = "*") => true;

			public IEnumerable<ValidationResult> ValidatePropertyValue(IDataEditor editor, IDataType dataType, object postedValue, bool isRequired, string validationRegExp, string isRequiredMessage, string validationRegExpMessage) => Array.Empty<ValidationResult>();

			public IEnumerable<ValidationResult> ValidatePropertyValue(IPropertyType propertyType, object postedValue) => Array.Empty<ValidationResult>();
		}
	}
}
