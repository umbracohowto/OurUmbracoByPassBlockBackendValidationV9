# OurUmbracoByPassBlockBackendValidation

In Umbraco version 9 and above, properties in the BlockList Editor that are marked as mandatory are validated both by the Frontend and the Backend.

We are going to explore the possibilities of bypassing this validation in the backend.


We are using a SurfaceController https://localhost:44315/umbraco/surface/migrateblockcontent

BackOffice credentials:
```
{
	"Umbraco backoffice user" : {
		"username": "test@test.com",
		"password": "Mgj]O}7o{p"
	}
}
```

